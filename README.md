# README #

The current README (as well as the library) is a work in progress, and will be updated in the future. 

### What is this repository for? ###

* This repository is meant to be used to easily create a file system that can interact with Local and Dropbox files in a similar way. In the future, other file types may be added
* Current version: 0.0.1

### How do I get set up? ###

* Setup requires pulling the library into your application, setting up a Dropbox app, and running some basic tests to ensure it is functioning properly

##Setup Dropbox
* Follow the instructions on https://www.dropbox.com/developers/core/start/ios to install the Core API in your application
    * This includes downloading and importing the SDK, creating a dropbox application, and modifying your AppDelegate. More explicit instructions to come later.

##TODO:
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* At this moment, in the early stages of development, there is no expected contribution or guidelines for it.

### Who do I talk to? ###

* Talk to the original developer @cadunne for questions, concerns, or suggestions for the library